import 'dart:io';

void main(List<String> arguments) {
  print("Input Word : ");
  String? input = stdin.readLineSync()!;
  List<String> wordList = input.toLowerCase().split(' ');
  var seen = Set<String>();
  List<String> uniqueWordList =
      wordList.where((word) => seen.add(word)).toList();
  for (var word in uniqueWordList) {
    print(word);
  }
  List<String> words = input.toLowerCase().split(' ');
  var map = {};
  for (var word in words) {
    if (!map.containsKey(word)) {
      map[word] = 1;
    } else {
      map[word] += 1;
    }
  }
  print(map);
}
